#!/usr/bin/env python3

import sys
import os
import functools
import argparse
import csv
from lxml import html
import requests

"""
This script scrapes basic stats about LittleWarGame units
from the game's Wiki page and converts it into a CSV table.
Warning: it is heavily dependent on the structure and
format of wiki pages! By the time you use this script,
it very well may not work!
"""

WIKI_ROOT = r"https://littlewargame.fandom.com"
WIKI_ENTRY_POINT = f"{WIKI_ROOT}/wiki/Littlewargame_Wiki"

def main():
    config = parse_args(sys.argv)

    main_page = requests.get(WIKI_ENTRY_POINT)
    page_tree = html.fromstring(main_page.content)
    unit_url_suffixes = page_tree.xpath('//*[@id="fpsubject2"]/center/div/a[not(img)]/@href')
    stats_per_unit = [parse_unit_stats(unit_name_from_url(suffix), f"{WIKI_ROOT}{suffix}") for suffix in unit_url_suffixes]

    with open_output_file(config.output) as out_file:
        dict_to_csv(stats_per_unit, output=out_file, delimiter=config.delimiter)

def parse_args(raw_args):
    parser = argparse.ArgumentParser(description="Parse some unit info from LWG wiki tables into CSV.")
    parser.add_argument("-o", "--output", required=False, type=str, default="-")
    parser.add_argument("-d", "--delimiter", required=False, type=str, default=";")

    return parser.parse_args()


def open_output_file(path):
    if path == "-":
        return os.fdopen(os.dup(sys.stdout.fileno()), mode="wt")

    return open(path, mode="wt", newline='')


def dict_to_csv(data_dicts, output=sys.stdout, delimiter=";"):
    field_names = functools.reduce(lambda d1, d2: {**dict.fromkeys(d1), **dict.fromkeys(d2)}, data_dicts)
    csv_writer = csv.DictWriter(output, fieldnames=field_names, delimiter=delimiter)
    csv_writer.writeheader()
    for stats in data_dicts:
         csv_writer.writerow(stats)

def parse_unit_stats(name, url):
    unit_record = {"Name": name}
    unit_page = requests.get(url)
    page_tree = html.fromstring(unit_page.content)
    stat_table_rows = page_tree.xpath('//h2[span[@id="Unit_Statistics"]]/following-sibling::table[1]/tbody/tr')
    for row in stat_table_rows:
        column_name, value = csv_column_from_wiki_table_row(row)
        unit_record[column_name] = value

    return unit_record

def unit_name_from_url(url):
    return url.split("/")[-1]

def csv_column_from_wiki_table_row(row):
    lines = row.text_content().split("\n")
    column_name = lines[1]
    value = " ".join(lines[2:-1])

    return column_name, value

if __name__ == "__main__":
    main()
