## Overview

This script compiles some info from LWG's Wiki into CSV.
With the proper field delimiter, it should be manageable by a spreadsheet tool
such as Microsoft Excel or LibreOffice Calc.

Platform-specific shenanigans may happen, such as encoding mismatch or
different treatment of newline characters between reader and writer.

It is probably a good idea to read the produced CSV using the UTF-8 encoding,
if presented with the choice.

## Prerequisites

Do not forget to install the dependencies listed in *requirements.txt*!

## Usage

This is a console script.
By default, output is to the console and can be redirected either using your shell of preference or the `-o` (`--output`) option.

See `--help` for more details.
